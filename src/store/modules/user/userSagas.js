import { put, all } from 'redux-saga/effects';
import * as types from './userTypes';

// worker saga: makes the api call when watcher saga sees the action
export function* fetchAllUserSaga(api) {
  try {
    // yield put(types.FETCH_ALL_BIN_REQUEST);
    // const response = yield call(api.users);
    const response = yield all(api.users);

    // dispatch a success action to the store with the new dog
    yield put({ type: types.FETCH_ALL_USER_SUCCESS, payload: response });
  } catch (error) {
    // dispatch a failure action to the store with the error
    yield put({ type: types.FETCH_ALL_USER_ERROR, payload: error });
  }
}

export default fetchAllUserSaga;
