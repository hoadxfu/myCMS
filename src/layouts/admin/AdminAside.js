import React, { PureComponent } from 'react';
import {
  List, ListItem, ListItemIcon, ListItemText, Icon,
} from '@material-ui/core';
import { withRouter } from 'react-router';
// import { Link } from 'react-router-dom';

const menu = [
  {
    name: 'Dashboard',
    path: '/admin/dashboard',
    icon: 'dashboard',
  },
  {
    name: 'Users',
    path: '/admin/users',
    icon: 'people',
  },
];

class AdminAside extends PureComponent {
  render() {
    const { history } = this.props;
    return (
      <div>
        <List component="nav">
          {menu.map(item => (
            <ListItem
              key={JSON.stringify(item)}
              button
              onClick={() => history.push(item.path)}
              // selected={location.pathname.includes(item.path)}
            >
              <ListItemIcon>
                <Icon>{item.icon}</Icon>
              </ListItemIcon>
              <ListItemText primary={item.name} />
            </ListItem>
          ))}
        </List>
      </div>
    );
  }
}

export default withRouter(AdminAside);
