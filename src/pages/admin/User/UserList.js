import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import {
  Typography,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Icon,
  IconButton,
  CircularProgress,
  Checkbox,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  withMobileDialog,
} from '@material-ui/core';
import { Link } from 'react-router-dom';

import { fetchAllUserAction } from '../../../store/modules/user/userActions';

const styles = theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  headline: {
    display: 'flex',
    paddingBottom: theme.spacing.unit * 2,
    alignItems: 'center',
  },
  headlineText: {
    display: 'inline-block',
    marginRight: '8px',
  },
  table: {
    minWidth: 700,
  },
  actionButton: {
    margin: theme.spacing.unit,
  },
});

class UserList extends PureComponent {
  state = {
    page: 0,
    rowsPerPage: 5,
    openConfirmDelete: false,
  };

  componentDidMount = () => {
    const { fetchAllUser } = this.props;
    fetchAllUser();
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleConfirmDeleteOpen = () => {
    this.setState({ openConfirmDelete: true });
  };

  handleConfirmDeleteClose = () => {
    this.setState({ openConfirmDelete: false });
  };

  render() {
    const { fullScreen, classes, users } = this.props;
    const { page, rowsPerPage, openConfirmDelete } = this.state;
    return (
      <div>
        <div className={classes.headline}>
          <Typography className={classes.headlineText} variant="headline" component="h3">
            Users
          </Typography>
          <Button component={Link} to="/admin/users/new" color="primary">
            Add New
          </Button>
        </div>
        <Paper className={classes.root} elevation={1}>
          {users.data ? (
            <div>
              <Table className={classes.table}>
                <TableHead>
                  <TableRow>
                    <TableCell padding="checkbox">
                      <Checkbox />
                    </TableCell>
                    <TableCell>Username</TableCell>
                    <TableCell>Name</TableCell>
                    <TableCell>Email</TableCell>
                    <TableCell />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {users.data
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(user => (
                      <TableRow key={JSON.stringify(user)}>
                        <TableCell padding="checkbox">
                          <Checkbox />
                        </TableCell>
                        <TableCell>{user.username}</TableCell>
                        <TableCell>{user.name}</TableCell>
                        <TableCell>{user.email}</TableCell>
                        <TableCell>
                          <IconButton aria-label="Edit">
                            <Icon>edit</Icon>
                          </IconButton>
                          <IconButton onClick={this.handleConfirmDeleteOpen} aria-label="Delete">
                            <Icon>delete</Icon>
                          </IconButton>
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
              <TablePagination
                component="div"
                count={users.data.length}
                rowsPerPage={rowsPerPage}
                page={page}
                backIconButtonProps={{
                  'aria-label': 'Previous Page',
                }}
                nextIconButtonProps={{
                  'aria-label': 'Next Page',
                }}
                onChangePage={this.handleChangePage}
                onChangeRowsPerPage={this.handleChangeRowsPerPage}
              />
            </div>
          ) : (
            <CircularProgress />
          )}
        </Paper>
        <Dialog
          fullScreen={fullScreen}
          open={openConfirmDelete}
          onClose={this.handleConfirmDeleteClose}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">Are you sure ?</DialogTitle>
          <DialogContent>
            <DialogContentText>This item will be permanently deleted.</DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleConfirmDeleteClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleConfirmDeleteClose} color="primary" autoFocus>
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = ({ userReducer }) => ({
  users: userReducer.list,
});

const mapDispatchToProps = dispatch => ({
  fetchAllUser: () => dispatch(fetchAllUserAction()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(withStyles(styles)(withMobileDialog()(UserList)));
